﻿using System.Collections.Generic;
using UnityEngine;

namespace RedPanda.Project
{
    [CreateAssetMenu(menuName = "Create IconsDatabase", fileName = "IconsDatabase", order = 0)]
    public class IconsDatabase : ScriptableObject
    {
        [SerializeField] private List<Sprite> Sprites;

        private Dictionary<string, Sprite> _cachedSprites;

        public Sprite GetSprite(string spriteName)
        {
            if (_cachedSprites == null) Init();
            if (_cachedSprites.ContainsKey(spriteName)) return _cachedSprites[spriteName];
            Debug.LogError($"Спрайта с названием {spriteName} нет в данной БД");
            return null;
        }
        
        private void Init()
        {
            _cachedSprites = new Dictionary<string, Sprite>(Sprites.Count);
            foreach (var sprite in Sprites)
            {
                _cachedSprites.Add(sprite.name, sprite);
            }
        }

    }
}