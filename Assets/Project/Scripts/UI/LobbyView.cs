using RedPanda.Project.Services.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace RedPanda.Project.UI
{
    public sealed class LobbyView : View
    {
        [SerializeField] private Button startButton;

        private void Awake()
        {
            //Example for services
            //var promoService = Container.Locate<IPromoService>();
            //UIService.Close();
            startButton.onClick.AddListener(OnStartButtonClick);
        }

        private void OnStartButtonClick()
        {
            UIService.Close("LobbyView");
            UIService.Show("PromoView");
        }
    }
}