﻿using System;
using System.Collections.Generic;
using System.Linq;
using RedPanda.Project.Data;
using RedPanda.Project.Interfaces;
using RedPanda.Project.Services;
using RedPanda.Project.Services.Interfaces;
using TMPro;
using UnityEngine;

namespace RedPanda.Project.UI
{
    public sealed class PromoView : View
    {
        [SerializeField] private TextMeshProUGUI currencyText;
        [SerializeField] private PromoElementView elementView;

        [SerializeField] private Transform chestsParent;
        [SerializeField] private Transform crystalsParent;
        [SerializeField] private Transform specialsParent;

        [SerializeField] private List<PromoElementViewRarityConfig> configs;

        private IShopService _shopService;
        
        
        private void Start()
        {
            UpdateCurrency();
            var promoService = Container.Locate<IPromoService>();
            var icons = Container.Locate<IconsDatabase>();
            _shopService = Container.Locate<IShopService>();
            var promos = promoService.GetPromos().OrderBy(x => x.Rarity);
            foreach (var promo in promos)
            {
                var rarityConfig = configs.Find(x => x.Rarity == promo.Rarity);
                var data = GetDataToInitElement(promo, rarityConfig, icons);
                var parentTransform = GetParentTransform(promo.Type);
                Instantiate(elementView, parentTransform).Init(data, () => OnPromoClick(promo));
            }
        }

        private void UpdateCurrency()
        {
            currencyText.text = $"{Container.Locate<IUserService>().Currency}";
        }

        private PromoElementViewData GetDataToInitElement(
            IPromoModel model, 
            PromoElementViewRarityConfig rarityConfig,
            IconsDatabase icons)
        {
            var result = new PromoElementViewData
            {
                Cost = model.Cost,
                Title = model.Title,
                RarityColor = rarityConfig.Color,
                BackgroundSprite = rarityConfig.BackgroundSprite,
                PromoElementIcon = icons.GetSprite(model.GetIcon())
            };

            return result;
        }

        private Transform GetParentTransform(PromoType type) => type switch
        {
            PromoType.Chest => chestsParent,
            PromoType.Special => specialsParent,
            PromoType.InApp => crystalsParent,
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
        };

        private void OnPromoClick(IPromoModel model)
        {
            _shopService.BuyItem(model);
            UpdateCurrency();
        }
    }

    public class PromoElementViewData
    {
        public int Cost;
        public string Title;
        public Color RarityColor;
        public Sprite BackgroundSprite;
        public Sprite PromoElementIcon;
    }
    
    [Serializable]
    public class PromoElementViewRarityConfig
    {
        public PromoRarity Rarity;
        public Color Color;
        public Sprite BackgroundSprite;
    }
}