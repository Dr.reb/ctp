﻿using System;
using DG.Tweening;
using RedPanda.Project.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RedPanda.Project.UI
{
    public class PromoElementView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI costText;
        [SerializeField] private Image backgroundImage;
        [SerializeField] private Image iconImage;
        [SerializeField] private Button elemButton;

        private Sequence _scaleAnim;

 
        public void Init(PromoElementViewData data, Action onClick)
        {
            transform.localScale = Vector3.one;
            gameObject.SetActive(true);
            costText.text = $"x{data.Cost}";
            nameText.text = data.Title;
            nameText.color = data.RarityColor;
            backgroundImage.sprite = data.BackgroundSprite;
            elemButton.onClick.AddListener(onClick.Invoke);
            elemButton.onClick.AddListener(PlayAnimation);
            iconImage.sprite = data.PromoElementIcon;
        }

        private void PlayAnimation()
        {
            if (_scaleAnim != null && _scaleAnim.active) return;
            _scaleAnim = DOTween.Sequence();
            _scaleAnim.Append(transform.DOScale(1.2f, 0.3f));
            _scaleAnim.Append(transform.DOScale(1f, 0.3f));
            _scaleAnim.OnComplete(() =>
            {
                _scaleAnim.Kill();
                _scaleAnim = null;
            });
        }

        private void OnDisable()
        {
            _scaleAnim?.Kill();
        }
    }
}