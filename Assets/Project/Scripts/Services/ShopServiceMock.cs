﻿using RedPanda.Project.Interfaces;
using RedPanda.Project.Services.Interfaces;
using UnityEngine;

namespace RedPanda.Project.Services
{
    public interface IShopService
    {
        void BuyItem(IPromoModel promoModel);
    }

    public class ShopServiceMock : IShopService
    {
        private readonly IUserService _userService;

        public ShopServiceMock(
            IUserService userService)
        {
            _userService = userService;
        }
        
        public void BuyItem(IPromoModel promoModel)
        {
            if (_userService.HasCurrency(promoModel.Cost))
            {
                _userService.ReduceCurrency(promoModel.Cost);
                Debug.LogWarning($"Куплен {promoModel.Title}");
            }
            else Debug.LogError("Попытка покупки без необходимого количества валюты");
        }
    }
}